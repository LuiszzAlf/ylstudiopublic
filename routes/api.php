<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\HorariosController;
use App\Http\Controllers\CitasController;
use App\Http\Controllers\CodigosController;
use App\Http\Controllers\UbicacionesController;
use App\Http\Controllers\PromocionesController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\ServiciosController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/loginc', [ClientesController::class, 'loginCliente']);
Route::post('/validateession', [ClientesController::class, 'ValidateSession']);
Route::get('/getclientes', [ClientesController::class, 'getClientes']);

// postcliente rute
// Parametros 
// nombre varchar
// correo email
// tel int
// state enuma (open,close)
Route::post('/postcliente', [ClientesController::class, 'postCliente']);
Route::post('/putcliente', [ClientesController::class, 'putCliente']);
Route::post('/deletecliente', [ClientesController::class, 'deleteCliente']);

Route::get('/gethorarios', [HorariosController::class, 'getHorarios']);
Route::get('/gethorariosdisponibles', [HorariosController::class, 'getHorariosDisponibles']);
Route::get('/gethorariosinfo', [HorariosController::class, 'getHorariosInfo']);
Route::get('/gethorariosvencidos', [HorariosController::class, 'getHorariosVencidos']);
Route::post('/posthorarios', [HorariosController::class, 'postHorarios']);
Route::post('/puthorarios', [HorariosController::class, 'putHorarios']);
Route::post('/deletehorarios', [HorariosController::class, 'deleteHorarios']);

Route::get('/getcitas', [CitasController::class, 'getCitas']);
Route::get('/getcita', [CitasController::class, 'getCita']);
Route::get('/getcitasadmin', [CitasController::class, 'getCitasAdmin']);
Route::get('/getcitasadminnow', [CitasController::class, 'getCitasAdminNow']);
Route::get('/getcitascli', [CitasController::class, 'getCitasCliente']);
Route::post('/postcita', [CitasController::class, 'postCita']);
Route::post('/putcita', [CitasController::class, 'putCita']);
Route::post('/deletecita', [CitasController::class, 'deleteCita']);
Route::post('/removecode', [CitasController::class, 'removeCode']);

Route::get('/getubicaciones', [UbicacionesController::class, 'getUbicaciones']);
Route::get('/getubicacionesdisp', [UbicacionesController::class, 'getUbicacionesDisponibles']);
Route::post('/postubicaciones', [UbicacionesController::class, 'postUbicaciones']);
Route::get('/getubicacionesdisp', [UbicacionesController::class, 'getUbicacionesDisponibles']);
Route::post('/putubicaciones', [UbicacionesController::class, 'putUbicaciones']);
Route::post('/deleteubicaciones', [UbicacionesController::class, 'deleteUbicaciones']);

Route::get('/getpromos', [PromocionesController::class, 'getPromociones']);
Route::post('/postpromociones', [PromocionesController::class, 'postPromociones']);
Route::post('/putpromociones', [PromocionesController::class, 'putPromociones']);
Route::post('/deletepromociones', [PromocionesController::class, 'deletePromociones']);

Route::get('/getusuarios', [UsuariosController::class, 'getUsuarios']);
Route::post('/putusuarios', [UsuariosController::class, 'putUsuarios']);
Route::post('/postusuarios', [UsuariosController::class, 'postUsuarios']);
Route::post('/deleteusuarios', [UsuariosController::class, 'deleteUsuarios']);
Route::post('/loginuser', [UsuariosController::class, 'loginUsers']);
// changestate rute
// Parametros
//id
Route::post('/changestate', [CitasController::class, 'changeState']);
Route::post('/changestateh', [HorariosController::class, 'changeStateH']);

// postcodigo rute
// Parametros 
// code varchar
// descuento varchar
// fecha_termino date
// cliente_id int
Route::post('/postcodigo', [CodigosController::class, 'postCodigos']);
Route::get('/getcodigos', [CodigosController::class, 'getcodigos']);
Route::get('/getcodigosdisponibles', [CodigosController::class, 'getCodigosDisponibles']);
Route::get('/validacodigo', [CodigosController::class, 'getCheckCodigo']);


// putcliente rute
// Parametros 
// code varchar
// descuento varchar
// fecha_termino date
// cliente_id int
// state enuma (open,close)
Route::post('/putcodigos', [CodigosController::class, 'putCodigos']);
Route::post('/deletecodigos', [CodigosController::class, 'deleteCodigos']);
//update cliente route
//Parametros
//nombre varchar
//correo varchar
//tel varchar
Route::post('/updatecliente', [ClientesController::class, 'updateCliente']);
//update fecha route
//Parametros
//fecha datetime
Route::post('/updatefecha', [HorariosController::class, 'updateFecha']);
// getClienteCodigo route
// parametros
// id int
Route::get('/getclientecodigo', [CodigosController::class, 'getClienteCodigo']);


Route::post('/postservicio', [ServiciosController::class, 'postServicio']);
Route::post('/putservicio', [ServiciosController::class, 'putServicio']);
Route::get('/getservicios', [ServiciosController::class, 'getServicios']);
Route::get('/getcatservicios', [ServiciosController::class, 'getServiciosDisponibles']);
Route::post('/deleteservicio', [ServiciosController::class, 'deleteServicio']);
