<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('/page/index');
});

Route::get('/citas', function () {
    return view('/page/apps');
});
Route::get('/mis_citas', function () {
    return view('/page/apps');
});
Route::get('/contacto', function () {
    return view('/page/contacto');
});

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//---------------------------- Rutas administrador ----------------------------------
Auth::routes();
Route::get('/admin/', function () {
    return view('auth/login');
});

Route::middleware('auth')->group(function () {
    Route::get('/home', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/home', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/citas', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/clientes', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/horarios', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/ubicaciones', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/promos', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/usuarios', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/web', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/codigos', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/horariosvencidos', function () {
        return view('/dashboard/index');
    });
    Route::get('/admin/servicios', function () {
        return view('/dashboard/index');
    });
});
