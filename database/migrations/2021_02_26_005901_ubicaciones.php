<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ubicaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ubicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('calle', 100)->nullable();
            $table->string('colonia', 100)->nullable();
            $table->string('ciudad', 100)->nullable();
            $table->string('estado', 100)->nullable();
            $table->integer('cp')->nullable();
            $table->string('numero', 100)->nullable();
            $table->string('maps', 100)->nullable();
            $table->enum('state', array('open','close'))->default('open');
            $table->json('r_object')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ubicaciones');
    }
}
