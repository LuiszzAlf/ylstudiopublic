<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Citas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Citas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('horario_id')->unsigned()->nullable();
            $table->integer('cliente_id')->unsigned();
            $table->integer('codigo_id')->unsigned()->nullable();
            $table->integer('servicio_id')->unsigned()->nullable();
            $table->foreign('horario_id')->references('id')->on('Horarios')->onDelete('cascade');
            $table->foreign('cliente_id')->references('id')->on('Clientes')->onDelete('cascade');
            $table->foreign('codigo_id')->references('id')->on('Codigos')->onDelete('cascade');
            $table->foreign('servicio_id')->references('id')->on('Servicios')->onDelete('cascade');
            $table->unique(['horario_id']);
            $table->string('folio', 100);
            $table->float('precio_final')->nullable();
            $table->enum('state', array('draft','confirmed','finish','paid','cancel'))->default('draft');
            $table->json('r_object')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Citas');
    }
}
