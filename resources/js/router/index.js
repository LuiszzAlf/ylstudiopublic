import Vue from "vue";
import VueRouter from "vue-router";
import codigos from "../components/Admin/Citas/Codigos.vue";
import citas from "../components/Admin/Citas/Citas.vue";
import clientes from "../components/Admin/Citas/Clientes.vue";
import horarios from "../components/Admin/Citas/Horarios.vue";
import ubicaciones from "../components/Admin/Citas/Ubicaciones.vue";
import promos from "../components/Admin/Citas/Promos.vue";
import usuarios from "../components/Admin/Citas/Usuarios.vue";
import web from "../components/Admin/Citas/Web.vue";
import citas_cli from "../components/Citas/Citas.vue";
import MisCitas from "../components/Citas/MisCitas.vue";
import horariosvencidos from "../components/Admin/Citas/HorariosVencidos.vue";
import servicios from "../components/Admin/Citas/Servicios.vue";
import inicio from "../components/Admin/Citas/Inicio.vue";
import AppLogin from "../components/Login/App.vue";

Vue.use(VueRouter);

const routes = [{
        path: "/home",
        name: "inicio",
        component: inicio,
    },
    {
        path: "/admin/codigos",
        name: "codigos",
        component: codigos,
    },
    {
        path: "/admin/horariosvencidos",
        name: "horariosvencidos",
        component: horariosvencidos,
    },
    {
        path: "/admin/citas",
        name: "citas",
        component: citas,
    },
    {
        path: "/admin/clientes",
        name: "clientes",
        component: clientes,
    },
    {
        path: "/admin/horarios",
        name: "horarios",
        component: horarios,
    },
    {
        path: "/admin/ubicaciones",
        name: "ubicaciones",
        component: ubicaciones,
    },
    {
        path: "/admin/promos",
        name: "promos",
        component: promos,
    },
    {
        path: "/admin/servicios",
        name: "servicios",
        component: servicios,
    },
    {
        path: "/admin/usuarios",
        name: "usuarios",
        component: usuarios,
    },
    {
        path: "/admin/web",
        name: "web",
        component: web,
    },
    {
        path: "/citas",
        name: "citas_cli",
        component: citas_cli,
    },
    {
        path: "/mis_citas",
        name: "MisCitas",
        component: MisCitas,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;