/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
window.Vue = require("vue").default;
import Vuetify from "vuetify";
import es from "vuetify/es5/locale/es";
import router from './router'
import Login from './components/Login/App.vue'
Vue.use(Vuetify);

const vuetify_lang = {
    theme: {
        options: {
            customProperties: true,
        },
        theme: { dark: true },
        themes: {
            light: {
                primary: "#2b81d6",
                headers: "#6c2526",
                secondary: "#424242",
                accent: "#82B1FF",
                error: "#FF5252",
                info: "#2196F3",
                success: "#4CAF50",
                warning: "#FFC107",
            },
        },
    },
    icons: {
        iconfont: "mdi",
    },
    lang: {
        locales: {
            es,
        },
        current: "es",
    },
};

const app = new Vue({
    el: "#app-admin",
    router,
    vuetify: new Vuetify(vuetify_lang),
});

const login = new Vue({
    el: "#login_component",
    vuetify: new Vuetify(),
    render: h => h(Login)
});

const login_m = new Vue({
    el: "#login_component_mobile",
    vuetify: new Vuetify(),
    render: h => h(Login)
});