<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('dashboard/assets/extra-libs/c3/c3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/dist/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/app.css') }}" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">

                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <div class="navbar-brand">
                        <a href="index.html">
                            <b class="logo-icon">
                                <img src="{{ asset('dashboard/assets/images/logo.png') }}" width="50px" alt="homepage"
                                    class="dark-logo" />

                                <img src="{{ asset('dashboard/assets/images/logo.png') }}" width="50px" alt="homepage"
                                    class="light-logo" />
                            </b>
                        </a>
                    </div>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1">
                    </ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <!-- <img src="{{ asset('dashboard/assets/images/users/profile-pic.jpg') }}" alt="user" -->
                                <!-- class="rounded-circle" width="40"> -->
                                <span class="ml-2 d-none d-lg-inline-block"><span>Hola,</span> <span
                                        class="text-dark">{{ Auth::user()->name }}</span> <i
                                        data-feather="chevron-down" class="svg-icon"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="user"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="credit-card"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="mail"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="settings"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                        data-feather="power" class="svg-icon mr-2 ml-1"></i>
                                    Salir</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/home') }}"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Inicio</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Administración</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/citas') }}"
                                aria-expanded="false"><i data-feather="calendar" class="feather-icon"></i><span
                                    class="hide-menu">Citas
                                </span></a>
                        </li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Catalogos</span></li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/clientes') }}"
                                aria-expanded="false"><i data-feather="user" class="feather-icon"></i><span
                                    class="hide-menu">Clientes
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/horarios') }}"
                                aria-expanded="false"><i data-feather="clock" class="feather-icon"></i><span
                                    class="hide-menu">Horarios
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/horariosvencidos') }}"
                                aria-expanded="false"><i data-feather="clock" class="feather-icon"></i><span
                                    class="hide-menu">Horarios Vencidos
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/ubicaciones') }}"
                                aria-expanded="false"><i data-feather="map" class="feather-icon"></i><span
                                    class="hide-menu">Ubicaciones
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/promos') }}"
                                aria-expanded="false"><i data-feather="gift" class="feather-icon"></i><span
                                    class="hide-menu">Promociones
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/codigos') }}"
                                aria-expanded="false"><i data-feather="star" class="feather-icon"></i><span
                                    class="hide-menu">Códigos
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/servicios') }}"
                                aria-expanded="false"><i data-feather="tag" class="feather-icon"></i><span
                                    class="hide-menu">Servicios
                                </span></a>
                        </li>
                        <li class="nav-small-cap"><span class="hide-menu">Configuracion</span></li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/usuarios') }}"
                                aria-expanded="false"><i data-feather="users" class="feather-icon"></i><span
                                    class="hide-menu">Usuarios
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin/web') }}"
                                aria-expanded="false"><i data-feather="layout" class="feather-icon"></i><span
                                    class="hide-menu">Web
                                </span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>

        @yield('content')
        <br><br><br>
        <footer class="footer text-center text-muted">
            <a href="https://irtransportacion.com/servicios/">{{ config('app.name', 'Laravel') }}</a>.

        </footer>

    </div>
    <script src="{{ asset('dashboard/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('dashboard/dist/js/app-style-switcher.js') }}"></script>
    <script src="{{ asset('dashboard/dist/js/feather.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}">
    </script>
    <script src="{{ asset('dashboard/dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('dashboard/dist/js/custom.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/extra-libs/c3/d3.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/extra-libs/c3/c3.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
