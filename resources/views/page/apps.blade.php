@extends('layouts.app_web')

@section('content')
<div data-app id="app-admin">
<router-view />
</div>
@endsection