@extends('layouts.app_web')

@section('content')
<div data-app id="app-admin">
    <router-view />
</div>
<div class=""> 

<section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="contact__content">
                        <div class="contact__address">
                            <h5>Información de contacto</h5>
                            <ul>
                                <li>
                                    <h6><i class="fa fa-map-marker"></i> Dirección</h6>
                                    <p> San Jose, 55654 Tequixquiac, Méx.</p>
                                </li>
                                <li>
                                    <h6><i class="fa fa-phone"></i> Teléfonos</h6>
                                    <p><span> 55 2419 8721</span></p>
                                </li>
                                <li>
                                    <h6><i class="fa fa-headphones"></i> Soporte</h6>
                                    <p>contacto@ylstudio.com.mx</p>
                                </li>
                            </ul>
                        </div>
                        <div class="contact__form">
                            <h5>ENVIAR MENSAJE</h5>
                            <form action="#">
                                <input type="text" placeholder="Nombre">
                                <input type="text" placeholder="Correo">
                                <textarea placeholder="Mensaje"></textarea>
                                <button type="submit" class="site-btn">ENVIAR MENSAJE</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3751.2555051700833!2d-99.13735204904113!3d19.913635030216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTnCsDU0JzQ5LjEiTiA5OcKwMDgnMDYuNiJX!5e0!3m2!1ses-419!2smx!4v1628899961783!5m2!1ses-419!2smx" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection