@extends('layouts.app_dashboard')

@section('content')
<div data-app id="app-admin">
<router-view />
</div>
@endsection