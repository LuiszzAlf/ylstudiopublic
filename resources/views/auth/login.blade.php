@extends('layouts.app_login')
@section('content')
<h2 class="mt-3 text-center">Acceso</h2>
<form class="mt-4" method="POST" action="{{ route('login') }}">
    <div class="row">
        {{ csrf_field() }}
        <div class="col-lg-12{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Correo</label>
            <div class="form-group">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                    autofocus>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-lg-12{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Contraseña</label>
            <div class="form-group">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        Recuérdame
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-12 text-center">
            <button type="submit" class="btn btn-block btn-dark">
                Entrar
            </button>
            <br>
            <a class="text-danger" href="{{ route('password.request') }}">
                ¿Olvidaste tu contraseña?
            </a>
        </div>

    </div>
</form>

@endsection