<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clientes extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    /**
     * The database connection
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * El nombre de la tabla donde se almacena los datos
     * @var String
     * @access protected
     */
    protected $table = 'Clientes';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
