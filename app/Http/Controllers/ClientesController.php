<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Clientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ClientesController extends Controller
{
    public function getClientes(Request $req)
    {
        $colums = array('id', 'nombre', 'correo', 'tel', 'state', 'r_object', 'created_at', 'updated_at');
        $Clientes = Clientes::select($colums)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Clientes,
        );
        return $array;
    }
    public function postCliente(Request $req)
    {
        $now = new \DateTime();

        $validate_email = Clientes::where('correo', $req['correo'])->get();
        $num = count($validate_email);
        if ($num > 0) {
            $state = 'error';
            $detail = 'El cliente ya existe';
            $response = [];
        } else {

            $Clientes = new Clientes;
            $Clientes->nombre = $req['nombre'];
            $Clientes->correo = $req['correo'];
            $Clientes->tel = $req['tel'];
            $Clientes->state = 'open';
            $Clientes->password = Hash::make($req['password']);
            $Clientes->r_object = '{}';
            $Clientes->save();
            $state = 'success';
            $detail = 'Cliente registrado con éxito! ';
            $response = $Clientes;
        }
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $response,
        );
        return $array;
    }

    public function putCliente(Request $req)
    {
        $now = new \DateTime();
        $ClientesPut = Clientes::where('id', $req['id'])
            ->update(
                ['nombre' => $req['nombre'],
                    'correo' => $req['correo'],
                    'tel' => $req['tel'],
                    'state' => $req['state'],
                    'updated_at' => $now->format('Y-m-d H:i:s'),
                ]
            );
        $state = 'success';
        $detail = 'actualizacion exitosa';
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $ClientesPut,
        );
        return $array;
    }
    public function updateCliente(Request $req)
    {
        $updateCliente = Clientes::where('id', $req['id'])
            ->update(
                [
                    'nombre' => $req['nombre'],
                    'correo' => $req['correo'],
                    'tel' => $req['tel'],
                ]
            );
        $state = 'success';
        $detail = 'actualizacion exitosa';
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $updateCliente,
        );
        return $array;
    }
    public function loginCliente(Request $req)
    {
        $now = new \DateTime();
        $password = '';
        $validate_email = Clientes::where('correo', $req['correo'])->get();
        foreach ($validate_email as $key => $val) {
            $password = $val->password;
        }
        if (Hash::check($req['password'], $password)) {
            $get_user = Clientes::where('correo', $req['correo'])->get(['id', 'nombre', 'correo', 'tel', 'state', 'updated_at', 'created_at', 'r_object']);
            $state = 'success';
            $detail = 'Bienvenido';
            $response = $get_user;
        } else {
            $state = 'error';
            $detail = 'Error de usuario y/o contraseña';
            $response = [];
        }
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $response,
        );
        return $array;
    }
    public function ValidateSession(Request $req)
    {
        $get_user = Clientes::where('correo', $req['correo'])->get(['id', 'nombre', 'correo', 'tel', 'state', 'updated_at', 'created_at', 'r_object']);
        if ($get_user!='[]') {
            $state = 'success';
            $detail = 'Ok';
            $response = $get_user;
        } else {
            $state = 'error';
            $detail = 'no se encontro el usuario o fue bloqueado';
            $response = 'null';
        }

        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $response,
        );
        return $array;
    }
    //
    public function deleteCliente(Request $req)
    {
        $delete_insert = Clientes::where([['id', '=', $req['id']]])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;
    }
}
