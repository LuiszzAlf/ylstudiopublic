<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Ubicaciones;
use Illuminate\Http\Request;


class UbicacionesController extends Controller
{
    public function getUbicaciones(Request $req)
    {
        $colums = array('*');
        $Ubicaciones = Ubicaciones::select($colums)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Ubicaciones,
        );
        return $array;
    }
    public function postUbicaciones(Request $req)
    {   
        $now = new \DateTime();

        $validar_ubicacion = Ubicaciones::where('id', $req['id'])->get();
        $num = count($validar_ubicacion);
        if($num > 0){
            $state = 'error';
            $detail = 'La ubicación ya existe';
            $response = [];
        }else{
        $Ubicaciones = new Ubicaciones;
        $Ubicaciones->calle = $req['calle'];
        $Ubicaciones->colonia = $req['colonia'];
        $Ubicaciones->ciudad = $req['ciudad'];
        $Ubicaciones->estado = $req['estado'];
        $Ubicaciones->cp = $req['cp'];
        $Ubicaciones->numero = $req['numero'];
        $Ubicaciones->maps = $req['maps'];
        $Ubicaciones->state = $req['state'];
        $Ubicaciones->r_object = '{}';
        $Ubicaciones->save();
        $state = 'sucess';
        $detail = 'Ubicación registrada';
        $response = $Ubicaciones;
        }
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $response,
        );
        return $array;

    }
    public function putUbicaciones(Request $req)
    {
        $now = new \DateTime();
        $UbicacionesPut = Ubicaciones::where('id', $req['id'])
            ->update(
                [
                'calle' => $req['calle'],
                'colonia' => $req['colonia'],
                'ciudad' => $req['ciudad'],
                'estado' => $req['estado'],
                'cp' => $req['cp'],
                'numero' => $req['numero'],
                'maps' => $req['maps'],
                'state' => $req['state'],
                'updated_at' => $now->format('Y-m-d H:i:s')
            ]);
            $state = 'sucess';
            $detail = 'actualización exitosa';
            $array = array(
                "state" => $state,
                "detail" => $detail,
                "data" => $UbicacionesPut,
            );
        return $array;
    }
    
    public function getUbicacionesDisponibles(Request $req)
    {
        $now = new \DateTime();
        $colums = array('*');
        $Ubicaciones = Ubicaciones::select($colums)->where('state','open')->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Ubicaciones,
        );
        return $array;
    }
    public function deleteUbicaciones(Request $req){
        $delete_insert = Ubicaciones::where([['id', '=', $req['id']],])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;
    }
}
