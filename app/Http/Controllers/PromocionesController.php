<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Promociones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PromocionesController extends Controller
{
    public function getPromociones(Request $req)
    {
        $colums = array('*');
        $Promociones = DB::connection('mysql')->select("SELECT p.id as id,p.servicio_id, p.fecha_inicio, p.fecha_fin, p.precio, p.descripcion, p.descuento, p.state, s.nombre as nombre_servicio
        FROM promociones p 
        INNER JOIN servicios s ON s.id = p.servicio_id");
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Promociones,
        );
        return $array;
    }
    public function postPromociones(Request $req)
    {
        $Promociones = new Promociones;
        $Promociones->servicio_id = $req['servicio_id'];
        $Promociones->fecha_inicio = $req['fecha_inicio'];
        $Promociones->fecha_fin = $req['fecha_fin'];
        $Promociones->precio = $req['precio'];
        $Promociones->descripcion = $req['descripcion'];
        $Promociones->descuento = $req['descuento'];
        $Promociones->state = $req['state'];
        $Promociones->r_object = '{}';
        $Promociones->save();
        $array = array(
            "state" => 'success',
            "detail" => 'Promoción creada con exito',
            "data" => $Promociones,
        );
        return $array;

    }
    public function putPromociones(Request $req){
        $now = new \DateTime();
        $promocionesPut = Promociones::where('id', $req['id'])
            ->update([
                'servicio_id' => $req['servicio_id'],
                'fecha_inicio' => $req['fecha_inicio'],
                'fecha_fin' => $req['fecha_fin'],
                'precio' => $req['precio'],
                'descripcion' => $req['descripcion'],
                'descuento' => $req['descuento'],
                'state' => $req['state'],
                'updated_at' => $now
            ]);
        $state = 'sucess';
        $detail = 'Promoción actualizada correctamente';
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $promocionesPut,
        );
        return $array;
    }
    public function getPromocionesDisponibles(Request $req)
    {
        $now = new \DateTime();
        $colums = array('*');
        $Promociones = Promociones::select($colums)->where('state', 'open')->where('fecha', '>=', $now)->get();
        $PromocionesDisponibles = DB::connection('mysql')->select("SELECT h.id,h.fecha ,h.ubicacion_id ,h.state ,h.r_object 
        ,h.created_at ,h.updated_at ,u.calle ,u.colonia ,u.ciudad,u.estado,u.maps from Promociones h
        inner join Ubicaciones u on u.id =h.ubicacion_id
        where h.state ='open' and h.fecha>=?", [$now]);
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $PromocionesDisponibles,
        );
        return $array;
    }
    public function deletePromociones(Request $req)
    {
        $delete_insert = Promociones::where([['id', '=', $req['id']],])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;

    }
}
