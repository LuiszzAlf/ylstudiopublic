<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Servicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ServiciosController extends Controller
{
    public function getServicios(Request $req)
    {
        $colums = array('*');
        $Servicios = Servicios::select($colums)->where('state', 'open')->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Servicios,
        );
        return $array;
    }
    public function getServiciosDisponibles(Request $req)
    {
        $colums = array('*');
        $Servicios = Servicios::select($colums)->where('state', 'open')->get();
        $items=[];
        foreach ($Servicios as $key => $val) {
            $item =   [ "id" => $val->id, "nombre" => $val->nombre.' '.$val->tipo.'/'.$val->descripcion];
            array_push($items,$item);
        }
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $items,
        );
        return $array;
    }
    public function postServicio(Request $req)
    {
        $now = new \DateTime();

        $validate_nombre = Servicios::where('nombre', $req['nombre'])->get();
        $num = count($validate_nombre);
        if ($num > 0) {
            $state = 'error';
            $detail = 'El servicio ya existe';
            $response = [];
        } else {
            $Servicios = new Servicios;
            $Servicios->nombre = $req['nombre'];
            $Servicios->tipo = $req['tipo'];
            $Servicios->descripcion = $req['descripcion'];
            $Servicios->precio = $req['precio'];
            $Servicios->r_object = '{}';
            $Servicios->save();
            $state = 'success';
            $detail = 'Servicio registrado con éxito! ';
            $response = $Servicios;
        }
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $response,
        );
        return $array;
    }

    public function putServicio(Request $req)
    {
        $now = new \DateTime();
        $ServiciosPut = Servicios::where('id', $req['id'])
            ->update(
                ['nombre' => $req['nombre'],
                    'tipo' => $req['tipo'],
                    'descripcion' => $req['descripcion'],
                    'precio' => $req['precio'],
                    'state' => $req['state'],
                    'updated_at' => $now->format('Y-m-d H:i:s'),
                ]
            );
        $state = 'success';
        $detail = 'actualizacion exitosa';
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $ServiciosPut,
        );
        return $array;
    }
   
    public function deleteServicio(Request $req)
    {
        $delete_insert = Servicios::where([['id', '=', $req['id']]])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;
    }
}
