<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User as Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    public function getUsuarios(Request $req)
    {
        $colums = array('*');
        $Usuarios = Usuarios::select($colums)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Usuarios,
        );
        return $array;
    }
    public function postUsuarios(Request $req)
    {
        $Usuarios = new Usuarios;
        $Usuarios->name = $req['name'];
        $Usuarios->email = $req['email'];
        $Usuarios->tipo = $req['tipo'];
        $Usuarios->password = $req['password'];
        $Usuarios->state = $req['state'];
        $Usuarios->r_object = '{}';
        $Usuarios->save();
        $array = array(
            "state" => 'success',
            "detail" => 'Usuario creado con exito',
            "data" => $Usuarios,
        );
        return $array;

    }
    public function putUsuarios(Request $req)
    {
        $UsuariosPut = Usuarios::where('id', $req['id'])
            ->update(
                [
                    'name' => $req['name'],
                    'email' => $req['email'],
                    'tipo' => $req['tipo'],
                    'state' => $req['state'],
                ]);
        $state = 'sucess';
        $detail = 'actualización exitosa';
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $UsuariosPut,
        );
        return $array;
    }
    public function getUsuariosDisponibles(Request $req)
    {
        $now = new \DateTime();
        $colums = array('*');
        $Usuarios = Usuarios::select($colums)->where('state', 'open')->where('fecha', '>=', $now)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Usuarios,
        );
        return $array;
    }
    public function deleteUsuarios(Request $req)
    {
        $delete_insert = Usuarios::where([['id', '=', $req['id']]])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;
    }

    public function loginUsers(Request $req)
    {
        $now = new \DateTime();
        $password = '';
        $validate_email = Usuarios::where('email', $req['correo'])->get();
        foreach ($validate_email as $key => $val) {
            $password = $val->password;
        }
        if (Hash::check($req['password'], $password)) {
            $get_user = Usuarios::where('email', $req['correo'])->get(['id', 'name', 'email', 'tipo', 'state', 'updated_at', 'created_at', 'r_object']);
            $state = 'success';
            $detail = 'Bienvenido';
            $response = $get_user;
        } else {
            $state = 'error';
            $detail = 'Error de usuario y/o contraseña';
            $response = [];
        }
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $response,
        );
        return $array;
    }
   

}
