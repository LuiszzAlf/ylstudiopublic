<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Codigos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CodigosController extends Controller
{
    public function getCodigos(Request $req)
    {
        $colums = array('*');
        $Codigos = Codigos::select($colums)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Codigos,
        );
        return $array;
    }
    public function postCodigos(Request $req)
    {
        $Codigos = new Codigos;
        $Codigos->code = $req['code'];
        $Codigos->descuento = $req['descuento'];
        $Codigos->fecha_termino = $req['fecha_termino'];
        $Codigos->cliente_id = $req['cliente_id'];
        $Codigos->state = $req['state'];
        $Codigos->r_object = '{}';
        $Codigos->save();
        $array = array(
            "state" => 'success',
            "detail" => 'Codigo creado con exito',
            "data" => $Codigos,
        );
        return $array;

    }
    public function putCodigos(Request $req)
    {
        $now = new \DateTime();
        $CodigosPut = Codigos::where('id', $req['id'])
            ->update(
                ['code' => $req['code'],
                    'descuento' => $req['descuento'],
                    'fecha_termino' => $req['fecha_termino'],
                    'cliente_id' => $req['cliente_id'],
                    'state' => $req['state'],
                ]
            );
        $state = 'sucess';
        $detail = 'Codigo actualizado correctamente';
        $array = array(
            "state" => $state,
            "detail" => $detail,
            "data" => $CodigosPut,
        );
        return $array;
    }
    public function getClienteCodigo(Request $req)
    {
        $nombreC = DB::connection('mysql')->select('SELECT a.nombre FROM clientes a INNER JOIN codigos b ON a.id = b.id WHERE b.id =?;', [$req['id']]);
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $nombreC,
        );
        return $array;
    }
    public function getCheckCodigo(Request $req)
    {
        $nombreC = Codigos::select('id', 'code', 'state', 'cliente_id')->where('code', '=', $req['codigo'])->where('state', '=', 'draft')->get();
        if (count($nombreC)>0) {
            $array = array(
                "state" => 'success',
                "detail" => 'Success',
                "data" => $nombreC,
            );
        } else {
            $array = array(
                "state" => 'error',
                "detail" => 'no se encontro el codigo',
                "data" => $nombreC,
            );
        }
        return $array;
    }
    // public function getCodigosDisponibles(Request $req)
    // {
    //     $now = new \DateTime();
    //     $colums = array('*');
    //     $Codigos = Codigos::select($colums)->where('state', 'open')->where('fecha', '>=', $now)->get();
    //     $CodigosDisponibles = DB::connection('mysql')->select("SELECT h.id,h.fecha ,h.ubicacion_id ,h.state ,h.r_object
    //     ,h.created_at ,h.updated_at ,u.calle ,u.colonia ,u.ciudad,u.estado,u.maps from Codigos h
    //     inner join Ubicaciones u on u.id =h.ubicacion_id
    //     where h.state ='open' and h.fecha>=?", [$now]);
    //     $array = array(
    //         "state" => 'success',
    //         "detail" => 'Success',
    //         "data" => $CodigosDisponibles,
    //     );
    //     return $array;
    // }

    public function deleteCodigos(Request $req)
    {
        $delete_insert = Codigos::where([['id', '=', $req['id']]])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;

    }

}
