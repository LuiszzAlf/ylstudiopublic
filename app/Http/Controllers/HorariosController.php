<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Horarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HorariosController extends Controller
{
    public function getHorarios(Request $req)
    {
        $now = new \DateTime();
        $colums = array('*');
        $Horarios = Horarios::select($colums)->where('fecha', '>=', $now)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Horarios,
        );
        return $array;
    }
    public function getHorarios2(Request $req)
    {
        $now = new \DateTime();
        $colums = array('*');
        $Horarios = Horarios::select($colums)->get();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $Horarios,
        );
        return $array;
    }
    public function getHorariosVencidos(Request $req){
        $now = new \DateTime();
        $colums = array('*');
        $Vencidos = Horarios::select($colums)->where('fecha', '<=', $now)->get();
        $array = array(
            "state" => 'sucess',
            "detail" => 'Sucess',
            "data" => $Vencidos,
        );
        return $array;
    }
    public function postHorarios(Request $req)
    {
        $Horarios = new Horarios;
        $Horarios->fecha = $req['fecha'];
        $Horarios->ubicacion_id = $req['ubicacion_id'];
        $Horarios->r_object = '{}';
        $Horarios->save();
        $array = array(
            "state" => 'success',
            "detail" => 'Horario creado con exito',
            "data" => $Horarios,
        );
        return $array;

    }
    public function putHorarios(Request $req){
        $now = new \DateTime();
        $HorariosPut = Horarios::where('id', $req['id'])
            ->update(
                [
                 'fecha' => $req['fecha'],
                 'ubicacion_id' =>$req['ubicacion_id'],
                 'updated_at' => $now->format('Y-m-d H:i:s')   
                ]);
            $state = 'sucess';
            $detail = 'actualización exitosa';
            $array = array(
                "state" => $state,
                "detail" => $detail,
                "data" => $HorariosPut,
            );
        return $array;
    }
    public function updateFecha(Request $req){
        $now = new \DateTime();
        $updateFecha = Horarios::where('id', $req['id'])
            ->update(
                [
                    'fecha' => $req['fecha'],
                    'updated_at' => $now->format('Y-m-d H:i:s')
                ]);
            $state = 'sucess';
            $detail = 'actualización exitosa';
            $array = array(
                "state" => $state,
                "detail" => $detail,
                "data" => $updateFecha,
            );
        return $array;
    }
    public function getHorariosDisponibles(Request $req)
    {
        $HorariosDisponibles = DB::connection('mysql')->select("SELECT h.id,h.fecha ,h.ubicacion_id ,h.state ,h.r_object 
        ,h.created_at ,h.updated_at ,u.calle ,u.colonia ,u.ciudad,u.estado,u.maps from Horarios h
        inner join Ubicaciones u on u.id =h.ubicacion_id
        where h.state ='open' and h.fecha >= CURDATE();");
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $HorariosDisponibles,
        );
        return $array;
    }
    public function changeStateH(Request $req){
        $now = new \DateTime();
        $change = Horarios::where('id', $req['id'])
            ->update(
                [
                    'state' => 'close',
                    'updated_at' => $now->format('Y-m-d H:i:s')
                ]
            );
            $state = 'sucess';
            $detail = 'actualización exitosa';
            $array = array(
                "state" => $state,
                "detail" => $detail,
                "data" => $change,
            );
        return $array;
    }
    public function deleteHorarios(Request $req)
    {
        $delete_insert = Horarios::where([['id', '=', $req['id']],])->delete();
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $delete_insert,
        );
        return $array;

    }
}
