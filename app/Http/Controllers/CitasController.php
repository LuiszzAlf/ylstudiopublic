<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Citas;
use App\Models\Horarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CitasController extends Controller
{
    public function getCitas(Request $req)
    {
        $colums = ['*'];
        $Citas = Citas::select($colums)->get();
        $array = [
            'state' => 'success',
            'detail' => 'Success',
            'data' => $Citas,
        ];
        return $array;
    }
    public function getCita(Request $req)
    {
        $colums = ['*'];
        $CitasCli = DB::connection('mysql')->select(
            "SELECT
                c.id as id,
                c.cliente_id ,
                c.horario_id,
                c.folio,
                c.state,
                h.fecha,
                u.calle ,
                u.colonia ,
                u.ciudad,
                u.estado ,
                u.maps,
                concat(s.nombre, ' ', s.tipo) servicio,
                s.precio precio_servicio,
                s.id servicio_id,
                c2.id codigo_id,
                c2.code codigo,
                c2.descuento descuento_codigo,
                p.descuento descuento_promocion,
                p.id promocion_id,
                p.descripcion descripcion_promocion,
                (COALESCE(p.descuento,0)+COALESCE(c2.descuento,0)) porcentaje_total,
                (CASE
                    WHEN ((COALESCE(p.descuento,0)+COALESCE(c2.descuento,0))) > 0
                    THEN s.precio-(s.precio*(COALESCE(p.descuento,0)+COALESCE(c2.descuento,0)) / 100)
                    ELSE s.precio
                END) as precio_final
            from
                Citas c
            inner join Horarios h on
                h.id = c.horario_id
            inner join Ubicaciones u on
                u.id = h.ubicacion_id
            inner join Servicios s on
                s.id = c.servicio_id
            left join Codigos c2 on
                c2.id = c.codigo_id
            left join Promociones p on
                p.servicio_id = s.id and p.state ='open'
            where
                c.deleted_at is null and 
                c.folio = ?;", [$req['folio']]);
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $CitasCli,
        );
        $array = [
            'state' => 'success',
            'detail' => 'Success',
            'data' => $CitasCli,
        ];
        return $array;
    }
    public function postCita(Request $req)
    {
        $validate = DB::connection('mysql')->select(
            "SELECT date(h.fecha) as fecha from Citas c
        inner join Horarios h on h.id = c.horario_id
        inner join Horarios h2 on h2.id = ?
        where c.cliente_id =? and date(h.fecha)=date(h2.fecha);",
            [$req['horario_id'], $req['cliente_id']]
        );

        if ($validate) {
            $state = 'error';
            $detail = 'No se puede agendar una cita el mismo día!';
            $response = $validate;
        } else {
            $Citas = new Citas();
            if ($req['codigo_id'] > 0) {
                $Citas->codigo_id = $req['codigo_id'];
            }
            if ($req['servicio_id'] > 0) {
                $Citas->servicio_id = $req['servicio_id'];
            }
            $Citas->folio = 'CIT-YL-' . rand();
            $Citas->horario_id = $req['horario_id'];
            $Citas->cliente_id = $req['cliente_id'];
            $Citas->state = 'draft';
            $Citas->r_object = '{}';
            $Citas->save();
            $state = 'success';
            $detail = 'Cita registrada con éxito! ';
            $response = $Citas;
            if ($response) {
                Horarios::where('id', $req['horario_id'])->update([
                    'state' => 'close',
                ]);
            }
        }
        $array = [
            'state' => $state,
            'detail' => $detail,
            'data' => $response,
        ];
        return $array;
    }
    public function putCita(Request $req)
    {
        $now = new \DateTime();
        if ($req['codigo_id'] == 'null') {
            $update_citas = Citas::where('id', $req['id'])->update([
                'cliente_id' => $req['cliente_id'],
                'horario_id' => $req['horario_id'],
                'servicio_id' => $req['servicio_id'],
                'updated_at' => $now->format('Y-m-d H:i:s'),
            ]);
        } else {
            $update_citas = Citas::where('id', $req['id'])->update([
                'cliente_id' => $req['cliente_id'],
                'horario_id' => $req['horario_id'],
                'servicio_id' => $req['servicio_id'],
                'codigo_id' => $req['codigo_id'],
                'updated_at' => $now->format('Y-m-d H:i:s'),
            ]);
        }

        $response = $update_citas;
        // cambiar estado del horario anterior a open y del horario nuevo a close
        if ($req['horario_act'] != $req['horario_id']) {
            Horarios::where('id', $req['horario_act'])->update([
                'state' => 'open',
            ]);
            Horarios::where('id', $req['horario_id'])->update([
                'state' => 'close',
            ]);
        }
        $state = 'Success';
        $detail = 'actualizacion exitosa';
        $array = [
            'state' => $state,
            'detail' => $detail,
            'data' => $response,
        ];
        return $array;
    }
    public function getCitasCliente(Request $req)
    {
        $colums = ['*'];
        $CitasCli = DB::connection('mysql')->select(
            "SELECT
                c.id as id,
                c.cliente_id ,
                c.horario_id,
                c.folio,
                c.state,
                h.fecha,
                u.calle ,
                u.colonia ,
                u.ciudad,
                u.estado ,
                u.maps,
                concat(s.nombre, ' ', s.tipo) servicio,
                s.precio precio_servicio,
                s.id servicio_id,
                c2.id codigo_id,
                c2.code codigo,
                c2.descuento descuento_codigo,
                p.descuento descuento_promocion,
                p.id promocion_id,
                p.descripcion descripcion_promocion,
                (COALESCE(p.descuento,0)+COALESCE(c2.descuento,0)) porcentaje_total,
                (CASE
                    WHEN ((COALESCE(p.descuento,0)+COALESCE(c2.descuento,0))) > 0
                    THEN s.precio-(s.precio*(COALESCE(p.descuento,0)+COALESCE(c2.descuento,0)) / 100)
                    ELSE s.precio
                END) as precio_final
            from
                Citas c
            inner join Horarios h on
                h.id = c.horario_id
            inner join Ubicaciones u on
                u.id = h.ubicacion_id
            inner join Servicios s on
                s.id = c.servicio_id
            left join Codigos c2 on
                c2.id = c.codigo_id
            left join Promociones p on
                p.servicio_id = s.id and p.state ='open'
            where
                c.deleted_at is null and 
                c.cliente_id = ?;", [$req['cliente_id']]);
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $CitasCli,
        );
        $array = [
            'state' => 'success',
            'detail' => 'Success',
            'data' => $CitasCli,
        ];
        return $array;
    }

    public function getCitasAdmin(Request $req)
    {
        $colums = ['*'];
        $CitasCli = DB::connection('mysql')
            ->select("SELECT
            c.id as id,
            c2.nombre,
            c2.correo,
            c2.tel,
            c.cliente_id,
            c.horario_id,
            c.servicio_id,
            c.codigo_id,
            concat(s.nombre, ' ', s.tipo) as servicio,
            co.code,
            c.folio,
            c.state,
            h.fecha,
            u.calle,
            u.colonia,
            u.ciudad,
            u.estado,
            u.maps
            from Citas c
            inner join Horarios h on h.id = c.horario_id
            inner join Ubicaciones u on u.id =h.ubicacion_id
            inner join Clientes c2  on c2.id = c.cliente_id
            left join Servicios s on s.id = c.servicio_id
            left join Codigos co on co.id = c.codigo_id
        where c.deleted_at is null;");
        $array = [
            'state' => 'success',
            'detail' => 'Success',
            'data' => $CitasCli,
        ];
        return $array;
    }

    public function getCitasAdminNow(Request $req)
    {
        $hoy = date('Y-m-d');
        $citasNow = DB::connection('mysql')
            ->select("SELECT c.id ,c2.nombre ,c2.correo ,c2.tel, c.cliente_id , c.horario_id, c.folio, c.state, h.fecha,u.calle ,u.colonia ,u.ciudad,u.estado ,u.maps  from Citas c
        inner join Horarios h on h.id = c.horario_id
        inner join Ubicaciones u on u.id =h.ubicacion_id
       inner join Clientes c2  on c2.id = c.cliente_id where c.deleted_at is null and  DATE_FORMAT(h.fecha,'%Y-%m-%d')='CURDATE()';");
        $array = array(
            "state" => 'success',
            "detail" => 'Success',
            "data" => $citasNow,
        );
        if (sizeof($citasNow) == 0) {
            return 'No hay citas';
        } else {
            return $citasNow;
        }
    }
    public function deleteCita(Request $req)
    {
        $delete_insert = Citas::where([['id', '=', $req['id']]])->delete();
        if ($delete_insert) {
            DB::update("UPDATE Citas set horario_id = null where id=?", [$req['id']]);
            Horarios::where('id', $req['horario_id'])->update([
                'state' => 'open',
            ]);
        }
        $array = [
            'state' => 'success',
            'detail' => 'Success',
            'data' => $delete_insert,
        ];
        return $array;
    }
    public function changeState(Request $req)
    {
        $now = new \DateTime();
        $change = Citas::where('id', $req['id'])
            ->update(
                [
                    'state' => $req['state'],
                    'updated_at' => $now->format('Y-m-d H:i:s'),
                ]
            );
        if ($req['state'] == 'cancel') {
            $PutState = DB::connection('mysql')->select("CALL SPHorariosLibera(?);", [$req['id']]);
        }

        $state = 'sucess';
        $detail = 'actualización exitosa';
        $array = [
            'state' => $state,
            'detail' => $detail,
            'data' => $change,
        ];
        return $array;
    }

    public function removeCode(Request $req)
    {
        $remove = Citas::where('id', '=', $req['id'])
            ->update([
                'codigo_id' => null,
            ]);
        $state = 'sucess';
        $detail = 'actualización exitosa';
        $array = [
            'state' => $state,
            'detail' => $detail,
            'data' => $remove,
        ];
        return $array;
    }
}
